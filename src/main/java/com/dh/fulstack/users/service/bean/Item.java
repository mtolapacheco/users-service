package com.dh.fulstack.users.service.bean;

/**
 * @author marvin tola
 */
public class Item {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
