package com.dh.fulstack.users.service.model.repositories;

import com.dh.fulstack.users.service.model.domain.Company;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author marvin tola
 */
public interface CompanyRepository extends JpaRepository<Company,Long> {
}