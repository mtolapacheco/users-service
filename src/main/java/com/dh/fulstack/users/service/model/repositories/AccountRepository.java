package com.dh.fulstack.users.service.model.repositories;

import com.dh.fulstack.users.service.model.domain.Account;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author marvin tola
 */
public interface AccountRepository extends JpaRepository<Account,Long> {


}
