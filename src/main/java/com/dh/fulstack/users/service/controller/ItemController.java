package com.dh.fulstack.users.service.controller;

import com.dh.fulstack.users.service.bean.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author marvin tola
 */
@RestController
@RequestMapping("/beans")
@RequestScope
public class ItemController {

    @Autowired
    private Item item;


//    public void Item(Item item) {
//        this.item = item;
//    }


    @RequestMapping(value = "/item", method = RequestMethod.GET)
    public Item readItem() {

        item.setName(item.getName() + ": GET");
        return item;

    }
}


