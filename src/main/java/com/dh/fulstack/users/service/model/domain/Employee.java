package com.dh.fulstack.users.service.model.domain;

import javax.persistence.*;

/**
 * @author marvin tola
 */
@Entity
@Table(name = "employee_table")
@PrimaryKeyJoinColumns({
        @PrimaryKeyJoinColumn(name = "employeeid",
                referencedColumnName = "userid")
})
//@DiscriminatorValue("emp") cuando se esta usando single

public class Employee extends User {

    /*@Id
    @Column(name = "employeeid", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    */

    @Column(name = "firstname", length = 50, nullable = false)
    private String firstName;

    /*
    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }
    */

    @Column(name = "lastanme", length = 50, nullable = false)
    private String lastName;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
