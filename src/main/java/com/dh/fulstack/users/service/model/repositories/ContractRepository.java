package com.dh.fulstack.users.service.model.repositories;

import com.dh.fulstack.users.service.model.domain.Contract;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author marvin tola
 */
public interface ContractRepository extends JpaRepository<Contract,Long> {
}
