package com.dh.fulstack.users.service.model.domain;

/**
 * @author marvin tola
 */
public enum AccountState {
    ACTIVATED,
    DEACTIVATED
}
