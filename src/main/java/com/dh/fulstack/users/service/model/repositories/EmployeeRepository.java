package com.dh.fulstack.users.service.model.repositories;

import com.dh.fulstack.users.service.model.domain.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author marvin tola
 */
public interface EmployeeRepository extends JpaRepository<Employee,Long>{
}
