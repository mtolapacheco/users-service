package com.dh.fulstack.users.service.config;

import com.dh.fulstack.users.service.bean.Asus;
import com.dh.fulstack.users.service.bean.Item;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

/**
 * @author marvin tola
 */

@Configuration

public class Config {

    @Bean
    @Scope("prototype")
    public Asus beanAsus(){
        Asus asus= new Asus();
        asus.setName("I am asus");

        return asus;
    }

    @Bean
    @Scope("prototype")
    public Item BeanItem() {

        Item item = new Item();
        item.setName("I am item");

        return item;
    }
}
